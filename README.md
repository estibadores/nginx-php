# nginx with PHP-FPM

Base image to run php applications. To use it place your php application in `/app` and if needed a setup script in `/etc/setup`.

The nginx is configured to listen for connections on port 8080.
