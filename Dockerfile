FROM registry.sindominio.net/nginx

RUN apt-get update && \
    apt-get -qy install php-fpm s6 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN  dpkg -s php-fpm | grep Version | cut -f2 -d" " | cut -d":" -f2 | cut -d"+" -f1 > /tmp/php-fpm.version

COPY *.conf /tmp/

RUN mv /tmp/nginx-service.conf /etc/nginx/sites-enabled/default

RUN PHP_VERSION=$(cat /tmp/php-fpm.version) && \
                cp /tmp/php-fpm.conf /etc/php/$PHP_VERSION/fpm/php-fpm.conf && \
                echo "include=/etc/php/$PHP_VERSION/fpm/pool.d/*.conf" >> /etc/php/$PHP_VERSION/fpm/php-fpm.conf && \
                cp /tmp/www.conf /etc/php/$(cat /tmp/php-fpm.version)/fpm/pool.d/www.conf && \
                ln -s /usr/sbin/php-fpm$PHP_VERSION /usr/sbin/php-fpm && \
                rm -rf /tmp/*.conf

ADD services.d /etc/services.d
ADD init /init
RUN chmod o+w /run

ENTRYPOINT ["/init"]
